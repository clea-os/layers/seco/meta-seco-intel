MACHINEOVERRIDES  =. "intel:x86-64:x86-x32:"
#MACHINE_FEATURES += "modem"
#MACHINE_HWCODECS:remove = "gstreamer1.0-vaapi"
#PACKAGECONFIG:append:pn-qtbase = " gles2"

# Da meta-seco
MACHINE_FEATURES += "va-impl-intel"
MACHINE_FEATURES += "alsa"

MACHINE_HWCODECS ?= "intel-vaapi-driver gstreamer1.0-vaapi"

MACHINE_EXTRA_RRECOMMENDS += "linux-firmware "

MACHINE_EXTRA_RDEPENDS += " kernel-modules"

IMAGE_FSTYPES += " wic wic.bmap"

WKS_FILE ?= "${@bb.utils.contains_any("EFI_PROVIDER", "systemd-boot", "systemd-bootdisk-microcode.wks.in", "grub-bootdisk-microcode.wks.in", d)}"
WKS_FILE_DEPENDS:append = " intel-microcode"

IMAGE_INSTALL:append = " eapi dmidecode afu libva libva-intel libva-intel-utils"

CORE_IMAGE_EXTRA_INSTALL+=" alsa-utils"

APPEND:append = "swiotlb=65536"

PACKAGECONFIG:append:pn-qtbase = " gles2"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-intel"

PREFERRED_VERSION_linux-intel = "6.1%"
PREFERRED_VERSION_linux-intel-rt = "6.1%"
PREFERRED_VERSION_linux-intel:poky-altcfg = "6.1%"
PREFERRED_VERSION_linux-intel-rt:poky-altcfg = "6.1%"


APPEND += "swiotlb=65536"
