/*
 * GPIO Chip driver for FXL6408 I/O Expander
 *
 * Copyright 2017 SECO srl
 *
 * Licensed under the GPL-2 or later.
 */
 
#include <linux/module.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/acpi.h>
#include <linux/interrupt.h>

#define DRV_NAME	"15740601"

#define NR_GPIO    8
#define CAN_SLEEP  0

#define FXL6408_DEVICE_ID		0x01
# define FXL6408_RST_INT		BIT(1)
# define FXL6408_SW_RST			BIT(0)

/* Bits set here indicate that the GPIO is an output. */
#define FXL6408_IO_DIR			0x03
/* Bits set here, when the corresponding bit of IO_DIR is set, drive
 * the output high instead of low.
 */
#define FXL6408_OUTPUT			0x05
/* Bits here make the output High-Z, instead of the OUTPUT value. */
#define FXL6408_OUTPUT_HIGH_Z		0x07
/* Bits here define the expected input state of the GPIO.
 * INTERRUPT_STAT bits will be set when the INPUT transitions away
 * from this value.
 */
#define FXL6408_INPUT_DEFAULT_STATE	0x09
/* Bits here enable either pull up or pull down according to
 * FXL6408_PULL_DOWN.
 */
#define FXL6408_PULL_ENABLE		0x0b
/* Bits set here (when the corresponding PULL_ENABLE is set) enable a
 * pull-up instead of a pull-down.
 */
#define FXL6408_PULL_UP			0x0d
/* Returns the current status (1 = HIGH) of the input pins. */
#define FXL6408_INPUT_STATUS		0x0f
/* Mask of pins which can generate interrupts. */
#define FXL6408_INTERRUPT_MASK		0x11
/* Mask of pins which have generated an interrupt.  Cleared on read. */
#define FXL6408_INTERRUPT_STAT		0x13

//#define CONFIG_GPIO_FXL6408_IRQ 1

/*
 * Debug Macros
 */
#define CGPIO_INFO(fmt, arg...) printk(KERN_INFO "SecoCGPIO: " fmt "\n" , ## arg)
#define CGPIO_ERR(fmt, arg...)  printk(KERN_ERR "%s: " fmt "\n" , __func__ , ## arg)
#define CGPIO_DBG(fmt, arg...)  pr_debug("%s: " fmt "\n" , __func__ , ## arg)


struct fxl6408_chip {
	struct gpio_chip gpio_chip;
	struct i2c_client *client;
	struct mutex i2c_lock;
	struct mutex lock;
	struct mutex lock_irq;
	uint8_t cache[8];
	struct irq_domain *irq_domain;
	int irq;
	u8 reg_io_dir;
	u8 reg_output;
};

static int fxl6408_gpio_direction_input(struct gpio_chip *gc, unsigned offset)
{
	struct fxl6408_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);
	chip->reg_io_dir &= ~BIT(offset);
	i2c_smbus_write_byte_data(chip->client, FXL6408_IO_DIR,
				  chip->reg_io_dir);
	mutex_unlock(&chip->i2c_lock);

	return 0;
}

static int fxl6408_gpio_direction_output(struct gpio_chip *gc,
		unsigned offset, int val)
{
	struct fxl6408_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);
	chip->reg_io_dir |=  BIT(offset);
	i2c_smbus_write_byte_data(chip->client, FXL6408_IO_DIR,
				  chip->reg_io_dir);
	mutex_unlock(&chip->i2c_lock);

	return 0;
}

static int fxl6408_gpio_get_direction(struct gpio_chip *gc, unsigned offset)
{
	struct fxl6408_chip *chip = gpiochip_get_data(gc);

	return (chip->reg_io_dir & BIT(offset)) == 0;
}

static int fxl6408_gpio_get_value(struct gpio_chip *gc, unsigned offset)
{
	struct fxl6408_chip *chip = gpiochip_get_data(gc);
	u8 reg;

	mutex_lock(&chip->i2c_lock);
	if((chip->reg_io_dir & BIT(offset)) != 0)
		reg = i2c_smbus_read_byte_data(chip->client, FXL6408_OUTPUT);
	else
		reg = i2c_smbus_read_byte_data(chip->client, FXL6408_INPUT_STATUS);
	mutex_unlock(&chip->i2c_lock);

	return (reg & BIT(offset)) != 0;
}

static void fxl6408_gpio_set_value(struct gpio_chip *gc, unsigned offset, int val)
{
	struct fxl6408_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);

	if (val)
		chip->reg_output |= BIT(offset);
	else
		chip->reg_output &= ~BIT(offset);

	i2c_smbus_write_byte_data(chip->client, FXL6408_OUTPUT,
				  chip->reg_output);
	mutex_unlock(&chip->i2c_lock);
}

static void fxl6408_gpio_set_multiple(struct gpio_chip *gc,
		unsigned long *mask, unsigned long *bits)
{
	struct fxl6408_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);
	chip->reg_output = (chip->reg_output & ~mask[0]) | bits[0];
	i2c_smbus_write_byte_data(chip->client, FXL6408_OUTPUT,
				  chip->reg_output);
	mutex_unlock(&chip->i2c_lock);
}

#ifdef CONFIG_GPIO_FXL6408_IRQ

#else

static irqreturn_t fxl6408_irq_handler(int irq, void *dev)
{
	int reg_interrupt;
	struct gpio_chip *gc = (struct gpio_chip *)(dev);
	struct fxl6408_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);

	reg_interrupt = i2c_smbus_read_byte_data(chip->client, FXL6408_INTERRUPT_STAT);
	if(reg_interrupt < 0)
		CGPIO_ERR("fxl6408_irq_handler i2c_smbus_read_byte_data failer with ret=%d", reg_interrupt);
		
	mutex_unlock(&chip->i2c_lock);
	
	return IRQ_HANDLED;
}

static int fxl6408_irq_setup(struct fxl6408_chip *chip)
{	
	int err;

	chip->irq_domain = irq_domain_add_linear(NULL, chip->gpio_chip.ngpio,
								&irq_domain_simple_ops, chip);
	if ( !chip->irq_domain ) {
		CGPIO_ERR ("cannot assign irq domain");
		err = -ENODEV;
		goto err_irq_domain_add;
	}


	err = devm_request_threaded_irq(&chip->client->dev, chip->client->irq, NULL, fxl6408_irq_handler, IRQF_TRIGGER_FALLING | IRQF_ONESHOT, dev_name(&chip->client->dev), chip);
	if (err != 0) {
		CGPIO_ERR ("fxl6408_irq_setup unable to request IRQ#%d: %d", chip->client->irq, err);
		goto err_request_thr_irq;
	}

	return 0;

err_request_thr_irq:
	irq_domain_remove (chip->irq_domain);
err_irq_domain_add:
	CGPIO_ERR("-fxl6408_irq_setup FAILED");
	return err;
}
#endif /* CONFIG_GPIO_FXl6408_IRQ */


static int fxl6408_exp_setup (struct fxl6408_chip *chip, struct device *dev,
							unsigned base) {
	int err, status;

	/* init gpio chip structure */
	chip->gpio_chip.get = fxl6408_gpio_get_value;
	chip->gpio_chip.set = fxl6408_gpio_set_value;
	chip->gpio_chip.direction_input = fxl6408_gpio_direction_input;
	chip->gpio_chip.direction_output = fxl6408_gpio_direction_output;

	chip->gpio_chip.get_direction = fxl6408_gpio_get_direction;
	chip->gpio_chip.set_multiple = fxl6408_gpio_set_multiple;

	chip->gpio_chip.ngpio = NR_GPIO;
	chip->gpio_chip.label = chip->client->name;

	chip->gpio_chip.base = base;
	chip->gpio_chip.can_sleep = CAN_SLEEP;
	chip->gpio_chip.parent = dev;
	chip->gpio_chip.owner = THIS_MODULE;

	mutex_init (&chip->lock);
	mutex_init (&chip->lock_irq);

	/* add this gpio bank */
	status = gpiochip_add_data(&chip->gpio_chip, chip);
	if ( status < 0 ) {
		CGPIO_ERR ("cannot add gpio bank to the system");
		err = status;
		goto err_gpio_add;
	}

	i2c_set_clientdata(chip->client, chip);

	status = fxl6408_irq_setup (chip);
	if ( status ) {
		CGPIO_ERR ("fxl6408_irq_setup cannot setup gpio irq");
		err = status;
		goto err_irq_setup;
	}

	return 0;
err_irq_setup:
err_gpio_add:
	CGPIO_ERR("-fxl6408_exp_setup FAILED");
	return err;
}


static int fxl6408_probe(struct i2c_client *client,
			 const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
	struct fxl6408_chip *chip;
	int err, ret;
	u8 device_id;
	unsigned base = -1;

	/* Check the device ID register to see if it's responding. */
	device_id = i2c_smbus_read_byte_data(client, FXL6408_DEVICE_ID);
	if (device_id < 0) {
		dev_err(dev, "FXL6408 probe returned %d\n", device_id);
		return device_id;
	} else if (device_id >> 5 != 5) {
		dev_err(dev, "FXL6408 probe returned DID: 0x%02x\n", device_id);
		return -ENODEV;
	}

	i2c_smbus_write_byte_data(client, FXL6408_OUTPUT_HIGH_Z, 0x00);
	i2c_smbus_write_byte_data(client, FXL6408_PULL_ENABLE, 0xFF);
	i2c_smbus_write_byte_data(client, FXL6408_PULL_UP, 0xFF);

	i2c_smbus_write_byte_data(client, FXL6408_IO_DIR, 0x00);
	i2c_smbus_write_byte_data(client, FXL6408_INPUT_DEFAULT_STATE, 0xFF);
	i2c_smbus_write_byte_data(client, FXL6408_INTERRUPT_MASK, 0x00);

	chip = devm_kzalloc(dev, sizeof(struct fxl6408_chip), GFP_KERNEL);
	if (!chip) {
		CGPIO_ERR ("cannot allocate memory for structure data");
		err = -ENOMEM;
		goto err_data_allocate;
	}

	chip->client = client;

	mutex_init(&chip->i2c_lock);
	chip->reg_io_dir = i2c_smbus_read_byte_data(client, FXL6408_IO_DIR);
	chip->reg_output = i2c_smbus_read_byte_data(client, FXL6408_OUTPUT);
	

	ret = fxl6408_exp_setup(chip, dev, base);
	if ( ret != 0 ) {
		CGPIO_ERR ("cannot driver setup");
		err = ret;
		goto err_setup;
	}

	return 0;

err_setup:
err_data_allocate:
	return err;
}

static int fxl6408_remove(struct i2c_client *client)
{
	struct fxl6408_chip *chip = i2c_get_clientdata(client);

#ifdef CONFIG_GPIO_FXL6408_IRQ
	fxl6408_exp_free_irq (chip);
#endif
	gpiochip_remove(&chip->gpio_chip);

	return 0;
}


static const struct i2c_device_id fxl6408_id[] = {
	{ DRV_NAME , 0 },
	{ },
};
MODULE_DEVICE_TABLE(i2c, fxl6408_id);

static const struct of_device_id fxl6408_of_match[] = {
	{ .compatible = "fcs,15740601" },
	{ }
};
MODULE_DEVICE_TABLE(of, fxl6408_of_match);

static struct acpi_device_id fxl6408_acpi_match[] = {
		{DRV_NAME, 0},
		{ },
};
MODULE_DEVICE_TABLE(acpi, fxl6408_acpi_match);

static struct i2c_driver fxl6408_driver = {
	.driver = {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = fxl6408_of_match,
		.acpi_match_table = ACPI_PTR(fxl6408_acpi_match),
	},
	.probe		= fxl6408_probe,
	.remove		= fxl6408_remove,
	.id_table	= fxl6408_id,
};

module_i2c_driver(fxl6408_driver);

MODULE_AUTHOR("Massimo Milanesi <m.massimomilanesi@gmail.com>");
MODULE_DESCRIPTION("GPIO expander driver for FXL6408");
MODULE_LICENSE("GPL");
