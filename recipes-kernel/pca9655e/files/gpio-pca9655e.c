/*
 * GPIO Chip driver for PCA9655E I/O Expander
 *
 * Copyright 2017 SECO srl
 *
 * Licensed under the GPL-2 or later.
 */
 
#include <linux/module.h>
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/acpi.h>
#include <linux/interrupt.h>
#include <asm/hw_irq.h>

#define DRV_NAME	"DI2C0001" // TODO: set the correct DeviceID

#define NR_GPIO    16
#define CAN_SLEEP  0
/*
 * PCAL6408 registers used by the GPIO device
 */
 
 
#define PCA9655E_INPUT_PORT0	0x00
#define PCA9655E_INPUT_PORT1	0x01
#define PCA9655E_OUTPUT_PORT0	0x02
#define PCA9655E_OUTPUT_PORT1	0x03
#define PCA9655E_CONFIG_PORT0	0x06 // Direction = 1 Input, 0 Output
#define PCA9655E_CONFIG_PORT1	0x07 // Direction = 1 Input, 0 Output


/*
 * Debug Macros
 */
#define CGPIO_INFO(fmt, arg...) printk(KERN_INFO "SecoCGPIO: " fmt "\n" , ## arg)
#define CGPIO_ERR(fmt, arg...)  printk(KERN_ERR "%s: " fmt "\n" , __func__ , ## arg)
#define CGPIO_DBG(fmt, arg...)  pr_debug("%s: " fmt "\n" , __func__ , ## arg)


struct gpio_exp_chip {
	struct gpio_chip gpio_chip;
	struct i2c_client *client;
	struct mutex i2c_lock;
	struct mutex lock;
	struct mutex lock_irq;
	uint8_t cache[8];
	struct irq_domain *irq_domain;
	int irq;
	u16 reg_io_dir;
	u16 reg_output;
};

static int gpio_exp_gpio_direction_input(struct gpio_chip *gc, unsigned offset)
{
	struct gpio_exp_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);
	chip->reg_io_dir &= ~BIT(offset);
	i2c_smbus_write_byte_data(chip->client, PCA9655E_CONFIG_PORT0, (~chip->reg_io_dir & 0x00FF));
	i2c_smbus_write_byte_data(chip->client, PCA9655E_CONFIG_PORT1, (~chip->reg_io_dir & 0xFF00) >> 8);
	mutex_unlock(&chip->i2c_lock);

	return 0;
}

static int gpio_exp_gpio_direction_output(struct gpio_chip *gc,
		unsigned offset, int val)
{
	struct gpio_exp_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);
	chip->reg_io_dir |= BIT(offset);
	i2c_smbus_write_byte_data(chip->client, PCA9655E_CONFIG_PORT0, (~chip->reg_io_dir & 0x00FF));
	i2c_smbus_write_byte_data(chip->client, PCA9655E_CONFIG_PORT1, (~chip->reg_io_dir & 0xFF00) >> 8);
	mutex_unlock(&chip->i2c_lock);

	return 0;
}

static int gpio_exp_gpio_get_direction(struct gpio_chip *gc, unsigned offset)
{
	struct gpio_exp_chip *chip = gpiochip_get_data(gc);
	return (chip->reg_io_dir & BIT(offset)) == 0;
}

static int gpio_exp_gpio_get_value(struct gpio_chip *gc, unsigned offset)
{
	struct gpio_exp_chip *chip = gpiochip_get_data(gc);
	u16 reg;

	mutex_lock(&chip->i2c_lock);
	reg = i2c_smbus_read_byte_data(chip->client, PCA9655E_INPUT_PORT0);
	reg += (i2c_smbus_read_byte_data(chip->client, PCA9655E_INPUT_PORT1) << 8);
	mutex_unlock(&chip->i2c_lock);

	return (reg & BIT(offset)) != 0;
}

static void gpio_exp_gpio_set_value(struct gpio_chip *gc, unsigned offset, int val)
{
	struct gpio_exp_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);

	if (val)
		chip->reg_output |= BIT(offset);
	else
		chip->reg_output &= ~BIT(offset);

	i2c_smbus_write_byte_data(chip->client, PCA9655E_OUTPUT_PORT0, (chip->reg_output & 0x00FF));
	i2c_smbus_write_byte_data(chip->client, PCA9655E_OUTPUT_PORT1, (chip->reg_output & 0xFF00) >> 8);
	
	mutex_unlock(&chip->i2c_lock);
}

static void gpio_exp_gpio_set_multiple(struct gpio_chip *gc,
		unsigned long *mask, unsigned long *bits)
{
	struct gpio_exp_chip *chip = gpiochip_get_data(gc);

	mutex_lock(&chip->i2c_lock);
	chip->reg_output = (chip->reg_output & ~mask[0]) | bits[0];
	i2c_smbus_write_byte_data(chip->client, PCA9655E_OUTPUT_PORT0, (chip->reg_output & 0x00FF));
	i2c_smbus_write_byte_data(chip->client, PCA9655E_OUTPUT_PORT1, (chip->reg_output & 0xFF00) >> 8);
	mutex_unlock(&chip->i2c_lock);
}

static int gpio_exp_exp_setup (struct gpio_exp_chip *chip, struct device *dev,
							unsigned base) {
	int err, status;

	/* init gpio chip structure */
	chip->gpio_chip.get = gpio_exp_gpio_get_value;
	chip->gpio_chip.set = gpio_exp_gpio_set_value;
	chip->gpio_chip.direction_input = gpio_exp_gpio_direction_input;
	chip->gpio_chip.direction_output = gpio_exp_gpio_direction_output;

	chip->gpio_chip.get_direction = gpio_exp_gpio_get_direction;
	chip->gpio_chip.set_multiple = gpio_exp_gpio_set_multiple;

	chip->gpio_chip.ngpio = NR_GPIO;
	chip->gpio_chip.label = chip->client->name;

	chip->gpio_chip.base = base;
	chip->gpio_chip.can_sleep = CAN_SLEEP;
	chip->gpio_chip.parent = dev;
	chip->gpio_chip.owner = THIS_MODULE;

	mutex_init (&chip->lock);
	mutex_init (&chip->lock_irq);

	/* add this gpio bank */
	status = gpiochip_add_data(&chip->gpio_chip, chip);
	if ( status < 0 ) {
		CGPIO_ERR ("cannot add gpio bank to the system");
		err = status;
		goto err_gpio_add;
	}

	i2c_set_clientdata(chip->client, chip);

	return 0;
err_gpio_add:
	CGPIO_ERR("-gpio_exp_exp_setup FAILED");
	return err;
}


static int gpio_exp_probe(struct i2c_client *client,
			 const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
	struct gpio_exp_chip *chip;
	int err, ret;
	unsigned base = -1;

	i2c_smbus_write_byte_data(client, PCA9655E_CONFIG_PORT0,	0xFF);
	i2c_smbus_write_byte_data(client, PCA9655E_CONFIG_PORT1,	0xFF);
	
	i2c_smbus_write_byte_data(client, PCA9655E_OUTPUT_PORT0, 	0x00);
	i2c_smbus_write_byte_data(client, PCA9655E_OUTPUT_PORT1, 	0x00);

	chip = devm_kzalloc(dev, sizeof(struct gpio_exp_chip), GFP_KERNEL);
	if (!chip) {
		CGPIO_ERR ("cannot allocate memory for structure data");
		err = -ENOMEM;
		goto err_data_allocate;
	}

	chip->client = client;

	mutex_init(&chip->i2c_lock);
	chip->reg_io_dir = i2c_smbus_read_byte_data(client, PCA9655E_CONFIG_PORT0);
	chip->reg_io_dir += (i2c_smbus_read_byte_data(client, PCA9655E_CONFIG_PORT1) << 8);
	chip->reg_io_dir = ~chip->reg_io_dir;

	chip->reg_output = i2c_smbus_read_byte_data(client, PCA9655E_INPUT_PORT0);
	chip->reg_output += (i2c_smbus_read_byte_data(client, PCA9655E_INPUT_PORT1) << 8);	

	ret = gpio_exp_exp_setup(chip, dev, base);
	if ( ret != 0 ) {
		CGPIO_ERR ("cannot driver setup");
		err = ret;
		goto err_setup;
	}

	return 0;

err_setup:
err_data_allocate:
	return err;
}

static int gpio_exp_remove(struct i2c_client *client)
{
	struct gpio_exp_chip *chip = i2c_get_clientdata(client);

	gpiochip_remove(&chip->gpio_chip);

	return 0;
}


static const struct i2c_device_id gpio_exp_id[] = {
	{ DRV_NAME , 0 },
	{ },
};
MODULE_DEVICE_TABLE(i2c, gpio_exp_id);

static const struct of_device_id gpio_exp_of_match[] = {
	{ },
	{ }
};
MODULE_DEVICE_TABLE(of, gpio_exp_of_match);

static struct acpi_device_id gpio_exp_acpi_match[] = {
		{DRV_NAME, 0},
		{ },
};
MODULE_DEVICE_TABLE(acpi, gpio_exp_acpi_match);

static struct i2c_driver gpio_exp_driver = {
	.driver = {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = gpio_exp_of_match,
		.acpi_match_table = ACPI_PTR(gpio_exp_acpi_match),
	},
	.probe		= gpio_exp_probe,
	.remove		= gpio_exp_remove,
	.id_table	= gpio_exp_id,
};

module_i2c_driver(gpio_exp_driver);

MODULE_AUTHOR("Massimo Milanesi <m.massimomilanesi@gmail.com>");
MODULE_DESCRIPTION("GPIO expander driver for PCA9655E");
MODULE_LICENSE("GPL");
