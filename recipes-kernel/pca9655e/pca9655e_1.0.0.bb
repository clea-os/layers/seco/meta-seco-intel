SUMMARY = "PCA9655e IO extender driver"
DESCRIPTION = "PCA9655e IO extender driver"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit module

PV = "1.0.1"
PR = "r0"

SRC_URI = "file://Makefile \
           file://gpio-pca9655e.c \
	   file://COPYING \
          "

S = "${WORKDIR}"
