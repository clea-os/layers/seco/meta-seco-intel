KERNEL_EXTRA_FEATURES = "features/netfilter/netfilter.scc"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
SRC_URI:append = " file://devmem.cfg \
                   file://i2c.cfg \
                   file://spi.cfg \
                   file://igc.cfg \
                   file://can.cfg \
                   file://pinctrl.cfg \
                   file://net-phy-dp83867.cfg \
                   file://squashfs.cfg \
                   file://docker.cfg \
"
