SUMMARY = ""
DESCRIPTION = ""
HOMEPAGE = ""

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PR = "r1"

FILES_${PN} += "${bindir}/afu"

PROVIDES = "afu"

TARGET_CC_ARCH += "${LDFLAGS}"

INSANE_SKIP:${PN} += "already-stripped"

SRC_URI = "git://git.seco.com/software/tools/amiutilities.git;protocol=https"
SRCREV = "13ae1cd6f7c1705c6ff7ba725f2ac11d5af9f2ab"

S = "${WORKDIR}/git"

RDEPENDS:${PN}-base = " make \
                        glibc \
                        ldd \
"

# Create /usr/bin in rootfs and copy program to it
do_install() {
    install -d ${D}${bindir}
    install -d ${D}${bindir}/afu/
    install -m 0755 ${S}/BIOS_update/AFU/Linux/x64/EtaAfuOemLnx64 ${D}${bindir}/afu
    install -m 0755 ${S}/BIOS_update/AFU/Linux/x64/update.sh ${D}${bindir}/afu
}
