To up-date the BIOS Flash on EHL boards, it is mandatory IN ONE SETUP SESSION:
- in the Boot menu be sure the first boot device is the one with the tools;
- Advanced-->PCH-FW Configuration-->Firmware Update Configuration-->Me FW Image Re-Flash = Enabled;
- Chipset-->PCH-IO Configuration-->Flash Protection Range Registers (FPRR) = Disabled.
Once saved, do the up-date procedure at the very next boot: these two last
settings  are one-shot.
Once finished, it is mandatory to power off and then on the system.