SUMMARY = ""
DESCRIPTION = ""
HOMEPAGE = ""

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

PR = "r1"

FILES_${PN} += "${bindir}/eapi"

PROVIDES = "eapi"

TARGET_CC_ARCH += "${LDFLAGS}"

INSANE_SKIP:${PN} += "already-stripped"

SRC_URI = "git://git.seco.com/software/eapi/eapi.git;protocol=https;branch=master"
SRCREV = "7cb34172ed624a5d65c5a75f7f4da7931d073855"

S = "${WORKDIR}/git"

# Create /usr/bin in rootfs and copy program to it
do_install() {
    install -d ${D}${bindir}
    install -d ${D}${libdir}/eapi/
    install -m 0755 ${S}/binary/linux64/EAPI_Test ${D}${bindir}
    install -m 0755 ${S}/binary/linux64/libEAPI.a ${D}${libdir}/eapi/
}
